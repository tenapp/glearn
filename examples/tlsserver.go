package main

import (
	"log"
	"net/http"
)

// A simple demonstration of how to use HTTPS
// in Go. Really, it's no different than a 
// normal HTTP server except for the inclusion
// of the cert and key in http.ListenAndServeTLS()

func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("TLS Server, yay."))
}

func main() {
	http.HandleFunc("/", handler)
	log.Printf("Server Started on port 10443 - localhost")
	err := http.ListenAndServeTLS(
		":10443",
		"place/server/cert/here",
		"place/server/key/here",
		nil)
	if err != nil {
		log.Fatal(err)
	}
}