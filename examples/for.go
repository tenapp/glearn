package main

import "fmt"

func main() {

	i := 1 // shorthand declaration
	for i <= 3 {
		fmt.Println(i)
		i = i + 1 // or i++
	}

	for j := 7; j <= 9; j++ {
		fmt.Println(j)
	}

	// infinate loop until you break it
	for {
		fmt.Println("loop")
		break
	}
}