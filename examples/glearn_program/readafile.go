package main 

import (
	"fmt"
	"io/ioutil"
)

func main() {
	var input string
	fmt.Printf("File to read:")
	fmt.Scanf("%s", &input)
	if input == "" {
		fmt.Println("You must enter a file name")
	}
	bs, err := ioutil.ReadFile(input)
	if err != nil {
		fmt.Sprintf("There was an error: No such file %v", err)
	}

	str := string(bs)
	fmt.Println(str)
}
