package main 

import (
	"fmt"
	"flag"
	"path/filepath"
	"os"
)

const (
	version string = "0.0.1 pre-alpha"
)

var (
	listDir = flag.String("l", "", "List directory")
)

func main() {
	flag.Parse()
	filepath.Walk(*listDir, func(
		path string,
		info os.FileInfo, err error) error {
		
		fmt.Println(path)
		return nil
	})
}

