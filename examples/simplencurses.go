package main

import (
	"code.google.com/p/goncurses"
)

func main() {
	stdscr, _ := goncurses.Init()
	defer goncurses.End()

	stdscr.Print("Hello, GoNcurses")
	stdscr.Refresh()
	stdscr.GetChar()
}
