package main

import (
	"log"
	"fmt"
	"net/http"
)

// Demonstration of how to write a very simple
// HTTP server in go.

func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	fmt.Fprintf(w, "Hello HTTP Server in Go.")
}

func main() {
	http.HandleFunc("/", handler)
	log.Printf("Server Started on port 8080 on localhost")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}