package main

import "fmt"

/* Slices
 * someArray := [10]float64{1,2,3,4,5,6,7,8,9,10}
 * x := make([]float, 5, 10)
 *                    ^   ^
 *        size of slice   capacity of array
 *
 * Another way to define using low : high
 * x := someArray[0:5]
 */

func main() {
	// Demonstrate append
	slice1 := []int{1,2,3}
	slice2 := append(slice1, 4, 5)
	fmt.Println(slice1, slice2)

	// Demonstrate copy
	slice3 := []int{1,2,3}
	slice4 := make([]int, 2)
	copy(slice4, slice3)
	fmt.Println(slice3, slice4)
	
}