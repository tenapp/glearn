package main

import (
	"fmt"
	"math"
)

type Circle struct {
	x, y, r float64
}

type Rectangle struct {
	x1, y1, x2, y2 float64
}

// A few ways to initialze the structure:
// Local Circle variable: var c Circle which
// the corresponding values are zero.
// Or create a pointer to the structure:
// c := new(Circle) - Again with all values
// zeroed.
	// More commonly, structures are used to
	// give values: c := Circle{x: 0, y: 0, r: 5}
//                  c := Circle{0, 0, 5}

// Calculate circle area using a one way
func circleArea(c *Circle) float64 {
	return math.Pi * c.r * c.r
}

func distance(x1, y1, x2, y2 float64) float64 {
	a := x2 - x1
	b := y2 - y1
	return math.Sqrt(a * a + b * b)
}

// Methods are a better way of doing things.
// Take in a receiver eg. func (c *Circle) funcName() type {}
func (c *Circle) area() float64 {
	return math.Pi * c.r * c.r
}

func (r *Rectangle) area() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)
	return l * w
}

func main() {
	c := Circle{0, 0, 5}
	r := Rectangle{0, 0, 10, 10}

	fmt.Println(circleArea(&c))
	fmt.Println(c.area())

	fmt.Println(r.area())
	
}