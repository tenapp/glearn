package main

import "fmt"

func average(xs []float64) float64 {
	total := 0.0
	for _, v := range xs {
		total += v
	}
	return total / float64(len(xs))
}

// Variadic function ... notation
func add(args ...int) int {
	total := 0
	for _, v := range args {
		total += v
	}
	return total
}

func makeEvenGenerator() func() uint {
	i := uint(0)
	return func() (ret uint) {
		ret = i
		i += 2
		return
	}
}

// Recursion
func factorial(x uint) uint {
	if x == 0 {
		return 1
	}

	return x * factorial(x - 1)
}

// Defer stuff
func first() {
	fmt.Println("1st")
}

func second() {
	fmt.Println("2nd")
}

func main() {
	xs := []float64{98,93,77,82,83}
	xp := []int{1,2,3}
	fmt.Println(average(xs))
	fmt.Println(add(1,2,3,4,5,6,7,8,9,22,33,44,44,55,566))
	fmt.Println(add(xp...))

	// Closure
	sub := func(x, y int) int {
		return x - y
	}
	fmt.Println(sub(3,2))

	nextEven := makeEvenGenerator()
	fmt.Println(nextEven())
	fmt.Println(nextEven())
	fmt.Println(nextEven())

	// defer example - move second() to end of function (i think)
	defer second()
	first()

	// Panic and Recover
	defer func() {
		str := recover()
		fmt.Println(str)
	}()
	panic("PANIC")
}
