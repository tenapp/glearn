package main

import "fmt"

func main() {
	x := make(map[string]int)
	x["key"] = 10
	fmt.Println(x["key"])
	delete(x, "key")
	fmt.Println(x["key"])

	i := make(map[int]int)
	i[1] = 10
	fmt.Println(i[1])

	// Better way to use maps (but not the best):
	elements := map[string]string {
		"H": "Hydrogen",
		"He": "Helium",
		"Li": "Lithium",
		"Be": "Beryllium",
		"B": "Boron",
		"C": "Carbon",
		"N": "Nitrogen",
		"O": "Oxygen",
		"F": "Fluorine",
		"Ne": "Neon",
	}

	fmt.Println(elements["H"])
}